document.querySelectorAll('nav a').forEach(anchor => {
      anchor.addEventListener('click', function (e) {
            e.preventDefault();

            const sectionId = this.getAttribute('href').substring(1);
            const section = document.getElementById(sectionId);

            if (section) {
                  section.scrollIntoView({
                        behavior: 'smooth'
                  });
            }
      });
});

window.addEventListener('scroll', function () {
      const navbar = document.getElementById('navbar');

      if (window.scrollY > window.innerHeight) {
            navbar.classList.add('show-navbar');
      } else {
            navbar.classList.remove('show-navbar');
      }
});
